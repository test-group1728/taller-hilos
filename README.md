# Uso de hilos en C

<br />

## ```lscpu```
```
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
Address sizes:       36 bits physical, 48 bits virtual
CPU(s):              4
On-line CPU(s) list: 0-3
Thread(s) per core:  2
Core(s) per socket:  2
Socket(s):           1
Vendor ID:           GenuineIntel
CPU family:          6
Model:               142
Model name:          Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz
Stepping:            9
CPU MHz:             2901.000
CPU max MHz:         2901.0000
BogoMIPS:            5802.00
Hypervisor vendor:   Windows Subsystem for Linux
Virtualization type: container
Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm pni pclmulqdq dtes64 est tm2 ssse3 fma cx16 xtpr pdcm pcid sse4_1 sse4_2 movbe popcnt aes xsave osxsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt ibrs ibpb stibp ssbd
```

<br />

## _Gráfico de Rendimiento_
![performance-graph](https://user-images.githubusercontent.com/34144827/130340880-8fcde55f-d308-4e36-811a-09b6d25b440a.jpg)

<br />

## _Análisis_
Como se puede apreciar en el gráfico, 7 hilos es el número adecuado para que este programa se ejecute de manera óptimo en una computadora con 4 CPUs. Según la gráfica, desde 1 hasta 4 hilos se puede afirmar que la relación de proporción entre el tiempo y el número de hilos es inversa. Sin embargo, con un número de 4 a 6 hilos se tiene una variación de tiempo mínima, y posterior a esto el comportamiento del programa y la gráfica es indefinido, por lo que se puede concluir que incrementar el número de hilos en la ejecución del programa es ineficiente.

<br />
