#include <stdio.h>
#include <png.h>

/* Parte del codigo tomado de https://gist.github.com/niw/5963798 */

#include <stdlib.h>
#include <stdio.h>
#include <png.h>
#include <stdint.h>
#include <pthread.h>
#include <bits/time.h>
#include <time.h>
#include <string.h>

typedef struct {
	size_t n_threads;	// number of threads
    size_t i;			// index
    int h;				// height
    int w;				// width
    png_bytep *row_p;	// row_pointers
    png_bytep *res;
} thread_data;

double obtenerTiempoActual(void) {
    struct timespec tsp;
    clock_gettime(CLOCK_REALTIME, &tsp);
    return ((double) (tsp.tv_sec) + ((double) tsp.tv_nsec / 1e9));
}

//Retorna los pixeles de la imagen, y los datos relacionados en los argumentos: ancho, alto, tipo de color (normalmente RGBA) y bits por pixel (usualemente 8 bits)
png_bytep * abrir_archivo_png(char *filename, int *width, int *height, png_byte *color_type, png_byte *bit_depth) {
	FILE *fp = fopen(filename, "rb");

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!png) return NULL;

	png_infop info = png_create_info_struct(png);
	if(!info) return NULL;

	if(setjmp(png_jmpbuf(png))) abort();

	png_init_io(png, fp);

	png_read_info(png, info);

	//resolucion y color de la image. Usaremos espacio de color RGBA
	*width      = png_get_image_width(png, info);
	*height     = png_get_image_height(png, info);
	*color_type = png_get_color_type(png, info);
	*bit_depth  = png_get_bit_depth(png, info);

	// Read any color_type into 8bit depth, RGBA format.
	// See http://www.libpng.org/pub/png/libpng-manual.txt

	if(*bit_depth == 16)
		png_set_strip_16(png);

	if(*color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

	// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
	if(*color_type == PNG_COLOR_TYPE_GRAY && *bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);

	if(png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

	// These color_type don't have an alpha channel then fill it with 0xff.
	if(*color_type == PNG_COLOR_TYPE_RGB ||
		*color_type == PNG_COLOR_TYPE_GRAY ||
		*color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

	if(*color_type == PNG_COLOR_TYPE_GRAY ||
		*color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	png_bytep *row_pointers;
	png_read_update_info(png, info);
	row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * (*height));
	if(row_pointers == NULL){
		printf("Error al obtener memoria de la imagen\n");
		return NULL;
	}

	for(int y = 0; y < *height; y++) {
		row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png,info));
	}

	png_read_image(png, row_pointers);

	fclose(fp);
	return row_pointers;
}

//Usaremos bit depth 8
//Color type PNG_COLOR_TYPE_GRAY_ALPHA
int guardar_imagen_png(char *filename, int width, int height, png_byte color_type, png_byte bit_depth, png_bytep *res) {
	FILE *fp = fopen(filename, "wb");
	if(!fp) return 1;

	png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) return 1;

	png_infop info = png_create_info_struct(png);
	if (!info) return 1;

	if (setjmp(png_jmpbuf(png))) return 1;

	png_init_io(png, fp);

	// Salida es escala de grises
	png_set_IHDR(
		png,
		info,
		width, height,
		bit_depth,
		color_type,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT
	);

	png_write_info(png, info);

	// To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
	// Use png_set_filler().
	//png_set_filler(png, 0, PNG_FILLER_AFTER);
	png_write_image(png, res);	//row_pointers
	png_write_end(png, NULL);
	for(int y = 0; y < height; y++) {
		free(res[y]);		//row_pointers
	}
	free(res);		//row_pointers

	fclose(fp);
	return 0;
}

void * procesar_fila(void *data){
	thread_data *t_data = (thread_data *) data;
    for(int x = t_data->i; x < t_data->h; x += t_data->n_threads) {
        png_bytep row = t_data->row_p[x];
        for(int y = 0; y < t_data->w; y++) {
            png_bytep pixel = &(row[y * 4]);
            t_data->res[x][2*y] = .299f * pixel[0] + .587f * pixel[1] + .114f * pixel[2];
            t_data->res[x][2*y + 1] = pixel[3];
        }
    }
    return t_data->res;
}

//Retorna el arreglo de pixeles
png_bytep * procesar_archivo_png(int width, int height, png_bytep *row_pointers, size_t n_threads) {
	//filas de nueva imagen
	png_bytep *res = malloc(sizeof(png_bytep *) * height);
	if(res == NULL){
		printf("No se pudo reservar espacio para la imagen procesada");
		return NULL;
	}

	for(int y = 0; y < height; y++) {
		//espacio para la los pixeles de dicha fila
		res[y] = malloc(sizeof(png_bytep) * width * 2);			//greyscale con alpha, 2 bytes por pixel

		if(res[y] == NULL){
			printf("No se pudo reservar espacio para la imagen procesada");
			return NULL;
		}
	}

	thread_data t_data[n_threads];
	pthread_t hilos[n_threads];
	double tiempoInicial, tiempoFinal, tiempoTotal;
	tiempoInicial = obtenerTiempoActual();
	for(size_t index = 0; index < n_threads; index++) {
		t_data[index].n_threads = n_threads;
		t_data[index].i = index;
		t_data[index].w = width;
		t_data[index].h = height;
		t_data[index].row_p = row_pointers;
		t_data[index].res = res;

		if (pthread_create(&hilos[index], NULL, procesar_fila, (void *)&t_data[index]) != 0)
			perror("Ha ocurrido un error al crear el hilo");
	}

    for (size_t i = 0; i < n_threads; ++i) {
        if (pthread_join(hilos[i], NULL) != 0)
            perror("Ha ocurrido un error al unir el hilo");
    }

    tiempoFinal = obtenerTiempoActual();
    tiempoTotal = tiempoFinal - tiempoInicial;
    printf("Tiempo total de ejecución: %.9lf\n", tiempoTotal);


	return res;
}

int main(int argc, char *argv[]) {
	if(argc != 4) abort();

	size_t num_threads = (size_t) strtoul(argv[3], NULL, 10);

	printf("Ejecutando %s con %zu hilos...\n", argv[0], num_threads);

  	//Datos de la imagen original
	int width, height;
	png_byte color_type; 
	png_byte bit_depth;
	png_bytep *pixeles;
	png_bytep *pixeles_res;

	pixeles = abrir_archivo_png(argv[1], &width, &height, &color_type, &bit_depth);
	if(pixeles == NULL) exit(1);

	pixeles_res = procesar_archivo_png(width, height, pixeles, num_threads);
	if(pixeles_res == NULL) exit(1);

	int res = guardar_imagen_png(argv[2], width, height, PNG_COLOR_TYPE_GRAY_ALPHA, bit_depth, pixeles_res);
	if(res == 1) exit(1);

	return 0;  
}
