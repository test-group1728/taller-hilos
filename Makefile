URL="https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74218/world.200412.3x21600x10800.png"

procesadorpng: procesadorpng.c
	gcc -Wall $^ -lpng -o $@ -pthread -g

correr: procesadorpng mundo.png
	./$^ bw.png 8
	file bw.png

mundo.png:
	curl $(URL) -o $@

.PHONY: clean
clean:
	rm -f procesadorpng bw.png
